import { Application } from 'express';

export interface Server {
    port: number | string;
    host: string;
    app: Application;
    server: any;
}
