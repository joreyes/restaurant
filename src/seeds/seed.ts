import { Connection } from 'typeorm';
import { Item } from '../app/Items/item.entity';
import { User } from '../app/User/user.entiry';
import { datatype, commerce, name } from 'faker/locale/es_MX';

const seedItems = async (connection: Connection) => {
    let items: Item[] = [];
    for (const _ of Array.from({ length: 10 })) {
        let item = new Item();
        (item.name = commerce.product()), (item.price = datatype.number());
        items.push(item);
    }
    let itemsSaved = await connection.manager.save(items);
    console.log(itemsSaved, 'Items saved success!');
    return;
};

const seedUsers = async (connection: Connection) => {
    let users: User[] = [];
    for (const _ of Array.from({ length: 10 })) {
        let user = new User();
        user.name = name.firstName();
        users.push(user);
    }
    let usersSaved = await connection.manager.save(users);
    console.log(usersSaved, 'Users saved success!');
    return;
};
export { seedItems, seedUsers };
