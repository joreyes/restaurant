import { EntityRepository, Repository } from 'typeorm';
import { Order } from './order.entity';

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
    getAllOrders() {
        return this.find();
    }

    createOrder(order: Order) {
        return this.save(order);
    }

    searchOrder(id: number) {
        return this.find({ id });
    }

    async updateStatus(id: number, status: string) {
        await this.update({ id }, { status });
        return this.findOne({ id });
    }
}
