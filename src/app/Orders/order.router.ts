import { Server as Main } from '../../interfaces/server';
import { Request, Response } from 'express';
import { getOrders, postOrder, putOrder } from './order.controller';

export default class RoutesOrders {
    constructor(private app: Main['app']) {}

    appRoutes() {
        this.app.get('/orders', (req: Request, res: Response) =>
            res.status(200).send('Orders routers')
        );
        this.app.get('/v1/orders', getOrders);
        this.app.post('/v1/orders', postOrder);
        this.app.put('/v1/orders/:id', putOrder);
    }
    routesConfig() {
        this.appRoutes();
    }
}
