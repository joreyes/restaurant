import { getCustomRepository } from 'typeorm';
import { Order } from './order.entity';
import { OrderRepository } from './order.repository';
import { ItemRepository } from '../Items/item.repository';
import { Item } from '../Items/item.entity';

export default class OrderService {
    iva: number = 16;
    constructor(
        private itemRepository = getCustomRepository(ItemRepository, 'db1'),
        private orderRepository = getCustomRepository(OrderRepository, 'db1')
    ) {}

    async Get(): Promise<Order[]> {
        return this.orderRepository.getAllOrders();
    }

    async createOrder(ids: number[]) {
        try {
            let items = await this.itemRepository.search(ids);
            if (items.length < ids.length) {
                return { error: 'Existen ids inválidos.' };
            }
            let { subtotal, vat, total, total_items } =
                this.applyOperations(items);
            let newOrder = await this.orderRepository.create({
                subtotal,
                vat,
                total,
                total_items,
                customer_name: 'sebastian',
                status: 'create',
                token: 'we345234f452f24r2',
                items
            });
            let order = await this.orderRepository.createOrder(newOrder);
            return { order };
        } catch (error) {
            console.log(error);
        }
    }

    async updateStatusOrder(id: number, status: string) {
        let existOrder = await this.orderRepository.searchOrder(id);
        if (existOrder.length > 0) {
            return {
                order: await this.orderRepository.updateStatus(id, status)
            };
        }
        return { error: `La orden con el id ${id} no existe.` };
    }

    applyOperations(items: Item[]) {
        let subtotal: number = 0;
        items.map(item => (subtotal = subtotal + item.price));
        let vat: number = (subtotal * this.iva) / 100;
        let total: number = vat + subtotal;
        return { subtotal, vat, total, total_items: items.length };
    }
}
