import { Request, Response } from 'express';
import OrderService from './order.service';

const getOrders = (req: Request, res: Response) => {
    let orderController = new OrderService();
    orderController
        .Get()
        .then(orders => res.json({ data: orders }))
        .catch(error => res.status(500).end());
};

const postOrder = (req: Request, res: Response) => {
    let { ids } = req.body;
    let orderController = new OrderService();
    orderController
        .createOrder(ids)
        .then(data => {
            if (data?.order) {
                res.status(201).json({ data: data.order });
            }
            res.status(201).json({ error: data?.error });
        })
        .catch(err => res.status(500).end());
};

const putOrder = (req: Request, res: Response) => {
    let { id } = req.params;
    let { status } = req.body;
    let orderController = new OrderService();
    orderController
        .updateStatusOrder(Number(id), status)
        .then(data => {
            if (data?.order) {
                res.status(200).json({ data: data.order });
            }
            res.status(404).json({ error: data?.error });
        })
        .catch(err => res.status(500).end());
};
export { getOrders, postOrder, putOrder };
