import {
    BaseEntity,
    Column,
    Entity,
    JoinTable,
    ManyToMany,
    PrimaryGeneratedColumn
} from 'typeorm';
import { Item } from '../Items/item.entity';

@Entity()
export class Order extends BaseEntity {
    @PrimaryGeneratedColumn()
    id!: number;
    @Column()
    subtotal!: number;
    @Column()
    vat!: number;
    @Column()
    total!: number;
    @Column()
    token!: string;
    @Column()
    total_items!: number;
    @Column()
    customer_name!: string;
    @Column()
    status!: string;
    @ManyToMany(type => Item)
    @JoinTable()
    items!: Item[];
}
