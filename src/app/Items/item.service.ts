import { getCustomRepository } from 'typeorm';
import { Item } from './item.entity';
import { ItemRepository } from './item.repository';

export default class ItemService {
    constructor(
        private itemRepository = getCustomRepository(ItemRepository, 'db1')
    ) {}

    async Get(): Promise<Item[]> {
        return await this.itemRepository.getAll();
    }
}
