import { Request, Response } from 'express';
import ItemService from './item.service';

const getItems = (req: Request, res: Response) => {
    let item = new ItemService();
    item.Get()
        .then(items => res.json({ data: items }))
        .catch(error => res.status(500).end());
};

export { getItems };
