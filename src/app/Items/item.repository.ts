import { EntityRepository, Repository, SelectQueryBuilder } from 'typeorm';
import { Item } from './item.entity';

@EntityRepository(Item)
export class ItemRepository extends Repository<Item> {
    getAll() {
        return this.find();
    }

    search(ids: number[]) {
        return this.findByIds(ids);
    }
}
