import { Server as Main } from '../../interfaces/server';
import { Request, Response } from 'express';
import { getItems } from './item.controller';

export default class RoutesItems {
    constructor(private app: Main['app']) {}

    appRoutes() {
        this.app.get('/items', (req: Request, res: Response) =>
            res.status(200).send('Items routers')
        );

        this.app.get('/v1/items', getItems);
    }
    routesConfig() {
        this.appRoutes();
    }
}
