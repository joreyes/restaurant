import express, { ErrorRequestHandler } from 'express';
import bodyParser from 'body-parser';
import http from 'http';
import cors from 'cors';

import { configuration } from './config/config';
import { Server as Main } from './interfaces/server';
import RoutesOrders from './app/Orders/order.router';
import RoutesItems from './app/Items/item.router';
import Connect from './config/db';

const { port } = configuration;
const app = express();
const server = new http.Server(app);
const host = 'localhost';

const main: Main = {
    port,
    app,
    server,
    host
};
class Server {
    constructor(private main: Main) {}

    appConfig() {
        this.main.app.use(bodyParser.json());
        this.main.app.use(bodyParser.urlencoded({ extended: true }));
        this.main.app.use(cors());
        // Error handling
        const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
            const { status, message } = err;
            console.error('error: ', message);
            res.status(status).end();
        };
        this.main.app.use(errorHandler);
    }

    includeRoutes() {
        new RoutesOrders(this.main.app).routesConfig();
        new RoutesItems(this.main.app).routesConfig();
    }

    initDB() {
        new Connect().connection();
    }

    appExecute() {
        this.appConfig();
        this.includeRoutes();
        this.initDB();
        const onListening = () =>
            console.log(`Server running in port ${this.main.port}`);
        this.main.server.listen(this.main.port, onListening);
    }
}
const start = new Server(main);
start.appExecute();
