import dotenv from 'dotenv';
dotenv.config();

export const configuration = {
    port: process.env.PORT || 3030,
    enviroment: process.env.NODE_ENV || 'production',
    seeds: process.env.SEEDS || 'false',
    db1: {
        name: process.env.DB_NAME,
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        database: process.env.DB_DATABASE,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD
    },
    db2: {
        name: process.env.DB2_NAME,
        host: process.env.DB2_HOST,
        port: Number(process.env.DB2_PORT),
        database: process.env.DB2_DATABASE,
        username: process.env.DB2_USERNAME,
        password: process.env.DB2_PASSWORD
    }
};
