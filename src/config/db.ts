import { Connection, createConnections } from 'typeorm';
import { User } from '../app/User/user.entiry';
import { configuration } from '../config/config';
import { seedItems, seedUsers } from '../seeds/seed';

export default class Connect {
    constructor() {}

    connection() {
        const { db1, db2, seeds } = configuration;
        createConnections([
            {
                name: 'db1',
                type: 'mysql',
                host: db1.host,
                port: db1.port,
                username: db1.username,
                password: db1.password,
                database: db1.database,
                entities: [__dirname + '/../**/*.entity{.ts,.js}'],
                synchronize: true
            },
            {
                name: 'db2',
                type: 'mysql',
                host: db1.host,
                port: db2.port,
                username: db2.username,
                password: db2.password,
                database: db2.database,
                entities: [User],
                synchronize: true
            }
        ])
            .then(async databases => {
                if (seeds === 'true') {
                    await seedItems(databases[0]);
                    await seedUsers(databases[1]);
                }
                console.log('connection to DB success');
            })
            .catch(error => console.log('connection to DB error', error));
    }
}
