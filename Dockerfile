FROM node:14.15.4-alpine3.12

# Create app directory
RUN mkdir -p /home/node/restaurant/node_modules && chown -R node:node /home/node/restaurant
WORKDIR /home/node/restaurant
# Install app dependencies
# where available (npm@5+)
COPY package*.json ./
USER node
RUN npm install
# Bundle app source
COPY --chown=node:node . .
RUN npm run build
EXPOSE 3000
CMD [ "node", "dist/server.js" ]